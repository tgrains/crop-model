# Function Call
```cpp
std::vector<int> getLandscapeIDs();
```

# Purpose
Returns a vector containing all IDs of the regions currently coded (for details see [getLandscapeString(...)](/TGRAINS/TGRAINS-Interface/getLandscapeString(...).md)).

# Argument Summary
Variable name | Intent | Purpose
-|- |-
