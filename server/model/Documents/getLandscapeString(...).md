# Function Call
```cpp
std::string getLandscapeString(int id);
```

# Purpose
Returns the name of region identified by `id`:
id | name
-|-
101| East Anglia Study Area
102|South Wales Study Area

# Argument Summary
Variable name | Intent | Purpose
-|-|-
id|input|Region ID.
